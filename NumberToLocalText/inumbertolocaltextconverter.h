/*!
 * This file contains declaration of interface for number to local text
 * conversion.
 *\file
 *\author Alexander Melihov <amelihovv@ya.ru>.
 */

#ifndef INUMBERTOTEXTLOCAL_H
#define INUMBERTOTEXTLOCAL_H

#include <QString>

/*!
 * Interface for number to local text conversion.
 */
class INumberToLocalTextConverter
{
public:
    virtual ~INumberToLocalTextConverter() {};

    /*!
     * Convert number to local text.
     *\param[in] number Number to convert.
     */
    virtual QString convert(quint64 number) = 0;
protected:
    /*!
     * Maximum length of 64 byte number (unsigned long long).
     */
    static const int DIGITS_COUNT = 19;
};

#endif // INUMBERTOTEXTLOCAL_H
