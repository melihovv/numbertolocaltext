#include <QtCore/QCoreApplication>
#include <QTextStream>
#include <QTextCodec>
#include "numbertolocaltextconverter.h"

int main(int argc, char *argv[])
{
    #ifdef Q_OS_WIN32
        QTextCodec::setCodecForLocale(QTextCodec::codecForName("IBM 866"));
    #endif

    NumberToLocalTextConverter converter;
    QTextStream out(stdout);
    out << converter.convert(123456, "Russian") << endl;

    return 0;
}
