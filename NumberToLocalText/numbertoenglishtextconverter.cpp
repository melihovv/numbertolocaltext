#include "numbertoenglishtextconverter.h"

NumberToEnglishTextConverter::NumberToEnglishTextConverter()
{

}

NumberToEnglishTextConverter::~NumberToEnglishTextConverter()
{

}

QString NumberToEnglishTextConverter::convert(quint64 number)
{
    QString result;

    if (number == 0)
    {
        return BEFORE_TWENTY[number];
    }

    QVector<int> threeDigitGroups;
    quint64 clone = number;

    int THREE_DIGIT_GROUP_COUNT = DIGITS_COUNT / 3;
    if (THREE_DIGIT_GROUP_COUNT * 3 < DIGITS_COUNT)
    {
        ++THREE_DIGIT_GROUP_COUNT;
    }

    for (int i = 0; i < THREE_DIGIT_GROUP_COUNT; ++i)
    {
        threeDigitGroups << clone % 1000;
        clone /= 1000;
    }

    QStringList groupsText;
    for (int i = 0; i < THREE_DIGIT_GROUP_COUNT; ++i)
    {
        groupsText << convertThreeDigitGroup(threeDigitGroups[i]);
    }

    result = groupsText[0];
    bool appendAnd = threeDigitGroups[0] > 0 && threeDigitGroups[0] < 100;

    for (int i = 1; i < THREE_DIGIT_GROUP_COUNT; ++i)
    {
        if (threeDigitGroups[i] != 0)
        {
            QString prefix = groupsText[i] + " " + SCALE[i - 1];
            if (result.length() != 0)
            {
                prefix += appendAnd ? " and " : ", ";
            }

            appendAnd = false;
            result = prefix + result;
        }
    }

    return result;
}

QString NumberToEnglishTextConverter::convertThreeDigitGroup(int threeDigit)
{
    QString result;
    int hundredDigit = threeDigit / 100;
    int tensUnit = threeDigit % 100;
    int tensDigit = tensUnit / 10;
    int unitsDigit = tensUnit % 10;

    if (hundredDigit != 0)
    {
        result += BEFORE_TWENTY[hundredDigit] + " hundred";

        if (tensUnit != 0)
        {
            result += " and ";
        }
    }

    if (tensDigit >= 2)
    {
        result += TENS[tensDigit - 2];

        if (unitsDigit != 0)
        {
            result += " " + BEFORE_TWENTY[unitsDigit];
        }
    }
    else if (tensUnit != 0)
    {
        result += BEFORE_TWENTY[tensUnit];
    }

    return result;
}
