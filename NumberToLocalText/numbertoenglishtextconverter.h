/*!
 * This file contains declaration of class for English text conversion.
 *\file
 *\author Alexander Melihov <amelihovv@ya.ru>.
 */

#ifndef NUMBERTOENGTEXT_H
#define NUMBERTOENGTEXT_H

#include <QStringList>
#include <QVector>
#include "inumbertolocaltextconverter.h"

/*!
 * Class for number to English text conversion.
 */
class NumberToEnglishTextConverter : public INumberToLocalTextConverter
{
public:
    NumberToEnglishTextConverter();
    ~NumberToEnglishTextConverter();

    QString convert(quint64 number);

private:
    QString convertThreeDigitGroup(int threeDigit);

    const QStringList BEFORE_TWENTY = QStringList
    {
        "zero", "one", "two", "three", "four", "five", "six",
        "seven", "eight", "nine", "ten", "eleven", "twelve",
        "thirteen", "fourteen", "fifteen", "sixteen",
        "seventeen", "eighteen", "nineteen"
    };

    const QStringList TENS = QStringList
    {
        "twenty", "thirty", "forty", "fifty",
        "sixty", "seventy", "eighty", "ninety"
    };

    const QStringList SCALE = QStringList
    {
        "thousand", "million", "billion",
        "trillion", "quadrillion", "quintillion"
    };
};

#endif // NUMBERTOENGTEXT_H
