#include "numbertolocaltextconverter.h"

NumberToLocalTextConverter::NumberToLocalTextConverter()
{
    this->converters =
    {
        {"English", new NumberToEnglishTextConverter()},
        {"Russian", new NumberToRussianTextConverter()}
    };
}

NumberToLocalTextConverter::~NumberToLocalTextConverter()
{

}

QString NumberToLocalTextConverter::convert(quint64 number, const QString &language)
{
    INumberToLocalTextConverter *converter = converters[language];
    return converter->convert(number);
}