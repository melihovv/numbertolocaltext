/*!
 * This file contains fabric of converters declaration.
 *\file
 *\author Alexander Melihov <amelihovv@ya.ru>.
 */

#ifndef NUMBERTOTEXTCONVERTER_H
#define NUMBERTOTEXTCONVERTER_H

#include <QString>
#include <QMap>
#include "numbertoenglishtextconverter.h"
#include "numbertorussiantextconverter.h"

/*!
 * Fabric of converters.
 */
class NumberToLocalTextConverter
{
public:
    NumberToLocalTextConverter();
    ~NumberToLocalTextConverter();

    /*!
     * Convert number to local text.
     *\param[in] number Number to convert.
     *\param[in] language Language of conversion result.
     *\return Converted text.
     */
    QString convert(quint64 number, const QString &language);

private:
    quint64 number;
    QMap<QString, INumberToLocalTextConverter *> converters;
};

#endif // NUMBERTOTEXTCONVERTER_H
