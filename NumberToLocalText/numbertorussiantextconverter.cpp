#include "numbertorussiantextconverter.h"

NumberToRussianTextConverter::NumberToRussianTextConverter()
{

}

NumberToRussianTextConverter::~NumberToRussianTextConverter()
{

}

QString NumberToRussianTextConverter::convert(quint64 number)
{
    if (number == 0)
    {
        return ZERO;
    }

    QVector<int> threeDigitGroups;
    quint64 clone = number;

    int THREE_DIGIT_GROUP_COUNT = DIGITS_COUNT / 3;
    if (THREE_DIGIT_GROUP_COUNT * 3 < DIGITS_COUNT)
    {
        ++THREE_DIGIT_GROUP_COUNT;
    }

    for (int i = 0; i < THREE_DIGIT_GROUP_COUNT; ++i)
    {
        threeDigitGroups << clone % 1000;
        clone /= 1000;
    }

    QStringList groupsText;
    for (int i = 0; i < THREE_DIGIT_GROUP_COUNT; ++i)
    {
        if (i == 1)
        {
            groupsText << convertThreeDigitGroup(threeDigitGroups[i], Gender::FEMININE);
        }
        else
        {
            groupsText << convertThreeDigitGroup(threeDigitGroups[i]);
        }
    }

    QString result = groupsText[0];

    for (int i = 1; i < THREE_DIGIT_GROUP_COUNT; ++i)
    {
        if (threeDigitGroups[i] != 0)
        {
            Form form;
            Case wordCase;

            int hundredDigit = threeDigitGroups[i] / 100;
            int tensUnit = threeDigitGroups[i] % 100;
            int tensDigit = tensUnit / 10;
            int unitsDigit = tensUnit % 10;

            if (unitsDigit == 1)
            {
                form = Form::SINGLE;
                wordCase = Case::NOMINATIVE;
            }

            if (unitsDigit >= 2 && unitsDigit <= 4)
            {
                if (i == 1)
                {
                    form = Form::PLURAL;
                    wordCase = Case::NOMINATIVE;
                }
                else
                {
                    form = Form::SINGLE;
                    wordCase = Case::GENITIVE;
                }
            }

            if (unitsDigit == 0 || unitsDigit >= 5)
            {
                form = Form::PLURAL;
                wordCase = Case::GENITIVE;
            }

            if (tensUnit >= 10 && tensUnit <= 19)
            {
                form = Form::PLURAL;
                wordCase = Case::GENITIVE;
            }

            QString groupText = groupsText[i] + " " + SCALE[(int) form][(int) wordCase][i - 1];
            if (result.length() != 0)
            {
                result = groupText + " " + result;
            }
            else
            {
                result += groupText;
            }
        }
    }

    return result;
}

QString NumberToRussianTextConverter::convertThreeDigitGroup(int threeDigit, const Gender &gender /*= Gender::MASCULINE*/)
{
    QString result;
    int hundredDigit = threeDigit / 100;
    int tensUnit = threeDigit % 100;
    int tensDigit = tensUnit / 10;
    int unitsDigit = tensUnit % 10;

    if (hundredDigit != 0)
    {
        result += HUNDRED[hundredDigit - 1];

        if (tensUnit != 0)
        {
            result += " ";
        }
    }

    if (tensDigit >= 2)
    {
        result += TENS[tensDigit - 2];

        if (unitsDigit != 0)
        {
            result += " " + DIGITS[(int) gender][unitsDigit - 1];
        }
    }
    else if (tensUnit != 0)
    {
        if (tensUnit < 10)
        {
            result += DIGITS[(int) gender][tensUnit - 1];
        }
        else
        {
            result += TEENS[tensUnit - 10];
        }
    }

    return result;
}
