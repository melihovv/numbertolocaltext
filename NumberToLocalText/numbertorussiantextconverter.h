/*!
 * This file contains declaration of class for Russian text conversion.
 *\file
 *\author Alexander Melihov <amelihovv@ya.ru>.
 */

#ifndef NUMBERTORUSSIANTEXTCONVERTER_H
#define NUMBERTORUSSIANTEXTCONVERTER_H

#include <QStringList>
#include <QVector>
#include <QList>
#include "inumbertolocaltextconverter.h"

enum class Gender
{
    MASCULINE,
    FEMININE
};

enum class Form
{
    SINGLE,
    PLURAL
};

enum class Case
{
    NOMINATIVE,
    GENITIVE
};

/*!
 * Class for number to Russian text conversion.
 */
class NumberToRussianTextConverter : public INumberToLocalTextConverter
{
public:
    NumberToRussianTextConverter();
    ~NumberToRussianTextConverter();

    QString convert(quint64 number);

private:
    QString convertThreeDigitGroup(int threeDigit, const Gender &gender = Gender::MASCULINE);

    const QString ZERO = "ноль";

    const QList<QStringList> DIGITS = QList<QStringList>
    {
        QStringList
        {
            "один", "два", "три", "четыре", "пять",
            "шесть", "семь", "восемь", "девять"
        },
        QStringList
        {
            "одна", "две", "три", "четыре", "пять",
            "шесть", "семь", "восемь", "девять"
        }
    };

    const QStringList TEENS = QStringList
    {
        "десять", "одиннадцать", "двенадцать", "тринадцать",
        "четырнадцать", "пятнадцать", "шестнадцать",
        "семнадцать", "восемнадцать", "девятнадцать"
    };

    const QStringList TENS = QStringList
    {
        "двадцать", "тридцать", "сорок", "пятьдесят",
        "шестьдесят", "семьдесят", "восемьдесят", "девяносто"
    };

    const QStringList HUNDRED = QStringList
    {
        "сто", "двести", "триста", "четыреста", "пятьсот",
        "шестьсот", "семьсот", "восемьсот", "девятьсот"
    };

    typedef QList<QStringList> LSL;
    const QList<LSL> SCALE = QList<LSL>
    {
        LSL
        {
            QStringList
            {
                "тысяча", "миллион", "миллиард", "триллион",
                "квадриллион", "квинтиллион"
            },
            QStringList
            {
                "тысячи", "миллиона", "миллиарда", "триллиона",
                "квадриллиона", "квинтиллиона"
            }
        },
        LSL
        {
            QStringList
            {
                "тысячи", "миллионы", "миллиарды", "триллионы",
                "квадриллионы", "квинтиллионы"
            },
            QStringList
            {
                "тысяч", "миллионов", "миллиардов", "триллионов",
                "квадриллионов", "квинтиллионов"
            }
        }
    };
};

#endif // NUMBERTORUSSIANTEXTCONVERTER_H
