# NumberToLocalText

Converts number to local text.
Now only two languages supports: English and Russian.
Qt framework is used.

## Features
- unsigned long long convertion

## Not supported
- Numbers with floating point
- Negative numbers

## Contributing
If you want to add any other language it's quite simple:

- add class, which will be implement `INumberToLocalTextConverter`
- implement `convert` method
- add expectations to existing test cases
- register your class in fabric class

That's all.