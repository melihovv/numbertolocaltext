#include <QtCore/QCoreApplication>
#include <QTest>
#include "testnumbertotextconverter.h"

int main(int argc, char *argv[])
{
    QTest::qExec(&TestNumberToTextConverter(), argc, argv);

    return 0;
}
