#ifndef TESTNUMBERTOTEXTCONVERTER_H
#define TESTNUMBERTOTEXTCONVERTER_H

#include <QObject>
#include <QTest>
#include <QTextCodec>
#include <QStringList>
#include "../NumberToLocalText/numbertolocaltextconverter.h"

typedef QMap<QString, QString> LocalExpectations;
typedef QPair<quint64, LocalExpectations> NumberAndExpectation;
typedef NumberAndExpectation NAE;
typedef QPair<QString, NAE> DescriptionAndNumberAndExpectation;
typedef DescriptionAndNumberAndExpectation DANAE;
typedef QList<DANAE> LanguageTestsGroup;

class TestNumberToTextConverter : public QObject
{
    Q_OBJECT

private slots:
    void testConvert_data();
    void testConvert();
};

#endif // TESTNUMBERTOTEXTCONVERTER_H
